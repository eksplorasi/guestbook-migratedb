const Guest = require('../models/Guest');

exports.getAll = function (req, res) {
    const responseJSON = { success: false, result: [], messages: [] }
    Guest.find({}).then((guest) => {
        if (guest) {
            responseJSON.success = true
            responseJSON.result = guest
            console.log('=> GET ALL GUESTS SUCCESS')
            res.status(200).json(responseJSON)
        } else {
            responseJSON.result = [err.message]
            return res.status(500).json(responseJSON)
        } 
    }).catch((err) => {
        responseJSON.result = [err.message]
        return res.status(500).json(responseJSON)
    })
}

exports.create = function (req, res) {
    const responseJSON = { success: false, result: [], messages: [] }

    NEW_DOC = {
        name: req.body.name,
        address: req.body.address
    }

    Guest.create(NEW_DOC).then((guest) => {
        if (guest) {
            responseJSON.success = true
            responseJSON.result = guest
            console.log('=> CREATE GUEST SUCCESS')
            res.status(200).json(responseJSON)
        } else {
            responseJSON.result = [err.message]
            return res.status(500).json(responseJSON)
        }  
    }).catch((err) => {
        responseJSON.result = [err.message]
        return res.status(500).json(responseJSON)
    })
}