const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const app = express()

// Mongoose
mongoose.connect('mongodb://localhost:27017/guestbook_migratedb', {useNewUrlParser: true}).then((infoDb) => {
    console.log('DB connection success')
}).catch((err) => {
    console.log('DB connection error', err)
});

// Middleware
app.use(express.json())
app.use(bodyParser.json({ limit: '5mb', type: 'application/json' }))
app.use(bodyParser.urlencoded({ extended: true }))

// Routes
const guestRoutes = require('./routes/guest')

app.use('/api/guest', guestRoutes)

// Run
app.listen(8000, () => {
  console.log('GUEST BOOK MIGRATE DB listening on port 8000')
})