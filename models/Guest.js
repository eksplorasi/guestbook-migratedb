const mongoose = require('mongoose')

const SCHEMA = new mongoose.Schema({
    name: String,
    address: String,
}, { timestamps: true })

module.exports = mongoose.model('Guest', SCHEMA)